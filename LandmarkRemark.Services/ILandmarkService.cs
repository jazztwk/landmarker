﻿using LandmarkRemark.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandmarkRemark.Services
{
    public interface ILandmarkService
    {
        /// <summary>
        /// Add new remark by user
        /// </summary>
        /// <param name="latitude">latitube</param>
        /// <param name="longitude">longitube</param>
        /// <param name="remark">remark</param>
        /// <param name="userId">userId</param>
        void AddRemark(double latitude, double longitude, string remark, string userId);
        /// <summary>
        /// Get all the remarks
        /// </summary>
        /// <returns></returns>
        List<LandmarkRemarkDto> GetRemarks();
        /// <summary>
        /// Search remark by username or remark
        /// </summary>
        /// <param name="query">query to search</param>
        /// <returns></returns>
        List<LandmarkRemarkDto> SearchRemark(string query);
    }
}
