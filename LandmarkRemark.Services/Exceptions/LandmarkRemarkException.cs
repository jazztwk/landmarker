﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandmarkRemark.Services.Exceptions
{
    public class LandmarkRemarkException : ApplicationException
    {
        public LandmarkRemarkException(string message) : base(message)
        {
        }

        public LandmarkRemarkException(Exception ex, string message):base(message,ex)
        {
        }
    }
}
