﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LandmarkRemark.Data.Entities;

namespace LandmarkRemark.Services.Dtos
{
    public class LandmarkRemarkDto
    {
        public LandmarkRemarkDto(LandmarkUserRemark entity)
        {
            Latitude = entity.Landmark.Location.Latitude.Value;
            Longitude = entity.Landmark.Location.Longitude.Value;
            Remark = entity.Remark;
            UserName = entity.User.UserName;
        }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string UserId { get; set; }
        public string Remark { get; set; }
        public string UserName { get; set; }
    }
}
