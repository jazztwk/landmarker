﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LandmarkRemark.Services.Dtos;
using LandmarkRemark.Data;
using LandmarkRemark.Data.Entities;
using System.Data.Entity.Spatial;
using LandmarkRemark.Services.Exceptions;

namespace LandmarkRemark.Services
{
    public class LandmarkService : ILandmarkService
    {
        public void AddRemark(double latitude, double longitude, string remark, string userId)
        {
            try
            {
                using (var context = new LandmarkContext())
                {
                    var user = context.Users.Find(userId);
                    if (user == null)
                        throw new LandmarkRemarkException("User not exist");
                    var landMarkRemark = new LandmarkUserRemark();
                    landMarkRemark.Remark = remark;
                    landMarkRemark.User = user;
                    landMarkRemark.CreatedDt = DateTime.Now;
                    landMarkRemark.Landmark = new Landmark()
                    {
                        CreatedDt = DateTime.Now,
                        Location = DbGeography.FromText(string.Format("POINT({0} {1})", longitude, latitude))
                    };
                    context.LandmarkUserRemarks.Add(landMarkRemark);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new LandmarkRemarkException(ex, ex.Message);
            }
        }

        public List<LandmarkRemarkDto> GetRemarks()
        {
            try
            {
                var allRemarks = new LandmarkContext().LandmarkUserRemarks.ToList();
                if (allRemarks.Count() > 0)
                    return allRemarks.Select(n => new LandmarkRemarkDto(n)).ToList();
                return new List<LandmarkRemarkDto>();
            }
            catch (Exception ex)
            {
                throw new LandmarkRemarkException(ex,ex.Message);
            }
        }

        public List<LandmarkRemarkDto> SearchRemark(string query)
        {
            try
            {
                using(var context=new LandmarkContext())
                {
                    var result = context.LandmarkUserRemarks
                        .Where(n => n.Remark.Contains(query) || n.User.UserName.Contains(query))
                        .ToList();
                    return result.Select(n => new LandmarkRemarkDto(n)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new LandmarkRemarkException(ex,ex.Message);
            }
        }
    }
}
