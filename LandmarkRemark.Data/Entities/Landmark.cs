﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LandmarkRemark.Data.Entities
{
    public class Landmark
    {
        [Key]
        public int LandmarkId { get; set; }
        public DbGeography Location { get; set; }
        public DateTime CreatedDt { get; set; }

        public virtual ICollection<LandmarkUserRemark> Remarks { get; set; }
        public Landmark()
        {
            this.Remarks = new HashSet<LandmarkUserRemark>();
        }
    }
}
