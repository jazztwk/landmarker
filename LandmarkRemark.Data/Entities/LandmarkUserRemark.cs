﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandmarkRemark.Data.Entities
{
    public class LandmarkUserRemark
    {
        [Key]
        public int LandmarkRemarkId { get; set; }
        public string UserId { get; set; }
        public string Remark { get; set; }
        public DateTime CreatedDt { get; set; }
        public int LandmarkId { get; set; }
        public virtual Landmark Landmark { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
