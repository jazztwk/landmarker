﻿using LandmarkRemark.Data.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace LandmarkRemark.Data
{
    public class LandmarkContext : IdentityDbContext<ApplicationUser>
    {
        public LandmarkContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static LandmarkContext Create()
        {
            return new LandmarkContext();
        }
        public DbSet<Landmark> Landmarks { get; set; }
        public DbSet<LandmarkUserRemark> LandmarkUserRemarks { get; set; }
    }
}
