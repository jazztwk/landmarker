﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LandmarkRemark.Models
{
    public class LandmarkAddNoteViewModel
    {
        [Required]
        [MaxLength(100)]
        [Display(Name = "Landmark Note")]
        public string Note { get; set; }
        [Required]
        [Display(Name = "Latitude")]
        public double? Lat { get; set; }
        [Required]
        [Display(Name = "Longitude")]
        public double? Lng { get; set; }
    }
}