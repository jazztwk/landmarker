﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LandmarkRemark.Services.Dtos;

namespace LandmarkRemark.Models
{
    public class LandmarkUserRemarkViewModel
    {
        public LandmarkUserRemarkViewModel(LandmarkRemarkDto dto)
        {
            UserName = dto.UserName;
            Remark = dto.Remark;
            Latitude = dto.Latitude;
            Longitude = dto.Longitude;
        }

        public double Latitude { get; private set; }
        public double Longitude { get; private set; }
        public string Remark { get; private set; }
        public string UserName { get; private set; }
    }
}