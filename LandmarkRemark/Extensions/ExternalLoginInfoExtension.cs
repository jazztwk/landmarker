﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LandmarkRemark.Extensions
{
    public static class ExternalLoginInfoExtension
    {
        /// <summary>
        /// Get profile picture from externel identity
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <returns></returns>
        public static string GetProfilePicture(this ExternalLoginInfo loginInfo)
        {
            return loginInfo.ExternalIdentity.Claims.Where(n => n.Type == MyClaimTypes.ProfilePicture).FirstOrDefault()?.Value;
        }
    }
}