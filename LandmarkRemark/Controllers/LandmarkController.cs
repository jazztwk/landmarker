﻿using LandmarkRemark.Models;
using LandmarkRemark.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Security.Principal;

namespace LandmarkRemark.Controllers
{
    [Authorize]
    public class LandmarkController : Controller
    {
        public ILandmarkService _landmarkService { get; private set; }
        public LandmarkController()
        {
            //Will use DI if time allow
            _landmarkService = new LandmarkService();
        }
        public IIdentity CurrentUser {
            get{
                return HttpContext.User.Identity;
            }
        }
        // GET: Landmark
        public ActionResult Index()
        {
            ViewBag.UserName = HttpContext.User.Identity.GetUserName();
            Session[MyClaimTypes.ProfilePicture] = HttpContext.GetOwinContext().Authentication.User.Claims.Where(n => n.Type == MyClaimTypes.ProfilePicture).First().Value;
            return View();
        }

        // POST: /Landmark/AddRemark
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddRemark(LandmarkAddNoteViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _landmarkService.AddRemark(viewModel.Lat.Value, viewModel.Lng.Value, viewModel.Note, CurrentUser.GetUserId());
                return Json(true);
            }
            return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest, GetModelStateError()));
        }

        // POST: /Landmark/SearchRemarks
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SearchRemarks(string query)
        {
            var result = _landmarkService.SearchRemark(query);
            return Json(result.Select(n => new LandmarkUserRemarkViewModel(n)));
        }
        // Get: /Landmark/GetRemarks
        [HttpGet]
        public JsonResult GetRemarks()
        {
            return Json(_landmarkService.GetRemarks().Select(n=>new LandmarkUserRemarkViewModel(n)),JsonRequestBehavior.AllowGet);
        }
        #region Helpers
        /// <summary>
        /// Get the first model error message
        /// </summary>
        /// <returns></returns>
        private string GetModelStateError()
        {
            return ModelState.Values.Where(n => n.Errors.Count > 0).Select(n => n.Errors.First().ErrorMessage).First();
        }
        #endregion
    }
}