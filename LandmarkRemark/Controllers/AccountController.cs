﻿using LandmarkRemark.Data.Entities;
using LandmarkRemark.Extensions;
using LandmarkRemark.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LandmarkRemark.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ApplicationSignInManager SignInManager
        {
            get{
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set{
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get{
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set{
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get{
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }
        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            if (!IsAuthenticate())
            {
                Session["Workaround"] = 0;
                // Request a redirect to the external login provider
                return new ChallengeResult("Google", Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
            }
            else
            {
                return RedirectToAction("Index", "Landmark");
            }
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Index","Home");
            }
            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                    // Has account login
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, create new account
                    var user = new ApplicationUser { UserName = loginInfo.Email, Email = loginInfo.Email };
                    var createdResult = await UserManager.CreateAsync(user);
                    if (createdResult.Succeeded)
                    {
                        createdResult = await UserManager.AddLoginAsync(user.Id, loginInfo.Login);
                        if (createdResult.Succeeded)
                        {
                            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                            //success
                            return RedirectToLocal(returnUrl);
                        }
                    }
                    //fail
                    AddErrors(createdResult);
                    return View("Error");
            }
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        #region Helpers
        /// <summary>
        /// Redirect to main page
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            //Go main page
            return RedirectToAction("Index", "Landmark");
        }

        /// <summary>
        /// add error to model state
        /// </summary>
        /// <param name="result"></param>
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        /// <summary>
        /// Check is current user already login
        /// </summary>
        /// <returns></returns>
        private bool IsAuthenticate()
        {
            return this.HttpContext.User != null &&
                this.HttpContext.User.Identity != null &&
                this.HttpContext.User.Identity.IsAuthenticated;
        }
        #endregion
    }
}