﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LandmarkRemark.Startup))]
namespace LandmarkRemark
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
