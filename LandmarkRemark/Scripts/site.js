﻿'use strict';

var map, userPos, allRemarks, markers = [];

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: true
    });
    map.addListener('click', function (e) {
        console.log(e);
    });
    fixInfoWindow();
    setUserLocation(map);
    displayAllRemarks(map);
    var addNoteButton = new button("Add Note", function () {
        showAddNoteDialog(map, userPos);
    });
    var myLocationButton = new button("My Location", function () {
        setUserLocation(map);
    });
    var searchBox = new textbox("Search Note or username", function (e) {
        var text = e.srcElement.value;
        if (text === "") {
            displayAllRemarks(map);
        } else {
            searchRemarks(text, map);
        }
    });;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(myLocationButton);
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(addNoteButton);
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(searchBox);
}

function setUserLocation(map) {
    var im = 'http://www.robotwoods.com/dev/misc/bluecircle.png';
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            userPos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var userMarker = new google.maps.Marker({
                position: userPos,
                map: map,
                icon: im
            });
            map.setCenter(userPos);
            map.setZoom(15);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function textbox(placeholder,onchange) {
    var controlDiv = document.createElement('div');
    controlDiv.className = 'controlDiv';
    var controlUI = document.createElement('div');
    controlUI.className = 'controlUI';
    var controlTextBox = document.createElement('input');
    controlDiv.appendChild(controlUI);
    controlTextBox.className = 'control controlTb';
    controlTextBox.placeholder = placeholder;
    controlUI.appendChild(controlTextBox);
    if (onchange) {
        controlTextBox.addEventListener('change',onchange);
    }
    return controlDiv;
};

function button(text, click) {
    var controlDiv = document.createElement('div');
    controlDiv.className = 'controlDiv';
    var controlUI = document.createElement('div');
    controlUI.className = 'controlUI';
    var controlText = document.createElement('div');
    controlDiv.appendChild(controlUI);
    controlText.className = 'control controlBtn';
    controlText.innerHTML = text
    controlUI.appendChild(controlText);
    if (click) { controlUI.addEventListener('click', click); }
    return controlDiv;
};

function addMarker(map,pos,data) {
    var marker = new google.maps.Marker({
        position: pos,
        map: map
    });
    marker.addListener('click', function (e) {
        console.log(e);
        var infowindow = new google.maps.InfoWindow({
            content: getWindowContent(data)
        });
        infowindow.open(map, this);
    });
    markers.push(marker);
    return marker;
};

function getWindowContent(data) {
    return  '<div><span class=\'remark-user\'>'+data.UserName+'</span><span> say</span></div>'+
            '<div><span class=\'remark-content\'>'+data.Remark+'</span></div>'
        ;
};

function addNote(note,map,pos) {
    var data = { note: note };
    data = $.extend(data, pos, { __RequestVerificationToken: getAntiForgeryToken() });
    $.post(addNoteUrl, data, function (result) {
        if (result.StatusCode) {
            showAlert(result.StatusDescription);
        } else {
            addMarker(map, pos, { UserName: currentUserName, Remark: note });
        }
    }).fail(function (err) {
        showAlert(err.statusText);
    });
};

function displayAllRemarks(map) {
    removeAllMarkers(map);
    $.get(getRemarksUrl, function (data) {
        allRemarks = data;
        if (data.length > 0) {
            $.each(data, function (index, value) {
                addMarker(map,{lat:value.Latitude,lng:value.Longitude},value)
            });
        }
    }).fail(function (err) {
        showAlert(err);
    });
};

function searchRemarks(query, map) {
    var data = $.extend({ query: query }, { __RequestVerificationToken: getAntiForgeryToken() });
    $.post(searchRemarksUrl, data, function (result) {
        removeAllMarkers(map);
        allRemarks = result;
        if (result.length > 0) {
            var bounds = new google.maps.LatLngBounds();
            $.each(result, function (index, value) {
                var marker = addMarker(map, { lat: value.Latitude, lng: value.Longitude }, value);
                bounds.extend(marker.position);
            });
            map.fitBounds(bounds);
        } else {
            showAlert("Ops! You found nothing.");
        }
    })
    .fail(function (err) {
        showAlert(err.statusText);
    });
};

function removeAllMarkers(map) {
    $.each(markers, function (index, value) {
        value.setMap(null);
    });
    markers = [];
};

function getAntiForgeryToken() {
    var form = $('#__AjaxAntiForgeryForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    return token;
};

function showAlert(msg) {
    bootbox.alert({ message: msg, backdrop :true});
};

function showAddNoteDialog(map,pos) {
    bootbox.prompt({
        title: "Add note to this location",
        callback: function (result) {
            if (result !== null) {
                addNote(result, map, pos);
            }
        }
    });
};
///Use set prototype to add new button
function fixInfoWindow() {
    var set = google.maps.InfoWindow.prototype.set;
    google.maps.InfoWindow.prototype.set = function (key, val) {
        var self = this;
        if (key === "map" && val!==null) {
            if (!this.get("noSupress")) {
                var position = this.position;
                var link = $("<a href='#' class='btn btn-primary'>Add note to this place</a>");
                link.click(function () {
                    showAddNoteDialog(map, { lat: position.lat(), lng: position.lng() });
                });
                $(this.content).append($("<div class=\"view-link add-note\"></div>").append(link));
            }
        } else if (key === "map") {
            //remove button
            $(this.content).find('.add-note').remove();
        }
        set.apply(this, arguments);
    }
}