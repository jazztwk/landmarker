﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LandmarkRemark
{
    /// <summary>
    /// Google Scope definition
    /// </summary>
    internal static class MyRequestedScopes
    {
        public static string[] Scopes
        {
            get
            {
                return new[] {
                    "openid",
                    "email",
                    "profile"
                };
            }
        }
    }
}