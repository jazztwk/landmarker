﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LandmarkRemark
{
    /// <summary>
    /// Custom claim type for google profile
    /// </summary>
    public static class MyClaimTypes
    {
        public const string GoogleUserId = "GoogleUserId";
        public const string ProfilePicture = "ProfilePicture";
    }
}